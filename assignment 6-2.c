#include <stdio.h>

int fibonacci(int n);
void fibonacciSeq(int n);

int main()
{
    int n;
    printf("Enter a number: ");
    scanf("%d", &n);
    fibonacciSeq(n);
    return 0;

}

int fibonacci(int n)
{
    if(n==0) return 0;
    else if(n==1) return 1;
    else return fibonacci(n - 1)+fibonacci(n-2);

}

void fibonacciSeq(int n){
    int i;
    for(i = 0; i<n;i++) {
      printf("%d\n", fibonacci(i));
   }
}
